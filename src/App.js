import './App.css';

import React from 'react';
import {useState, useEffect, Component} from 'react';


// API Candidates Brainstorming:
// - https://opentdb.com/api_config.php
// - ...
const QUIZ_API_BASE_URL = undefined; // TODO: Incorporate Questions API

const MOCK_QUIZ = [
  {
    "question": "When was the programming language 'C#''; released?",
    "answers": [
      "1998",
      "2000",
      "1999",
      "2001",
    ],
    "correctAnswer": 1
  },
  {
    "question": "In any programming language, what is the most common way to iterate through an array?",
    "answers": [
      "Clipper Chip",
      "Enigma Machine",
      "Skipjack",
      "Nautilus",
    ],
    "correctAnswer": 0
  },
  {
    "question": "How many Hz does the video standard PAL support?",
    "answers": [
      "59",
      "60",
      "50",
      "25",
    ],
    "correctAnswer": 2,
  },
];


/*
NOTE:
- First React App, meant as a way to practice the main Concepts.
  - May consider Context later, Optimizations with Memo, or other Concepts.
  - Also, breaking up Components into separate files, more robust error handling, etc.
- First pass is basic Implementation, not breaking out into Components yet.
- Below Approach is Desired State, but not complete yet


HOW TO USE:
In App's Root Directory:
- Run "npm install"
- Run "npm start"


### Design

##### REQUIREMENTS:
- Obtain Quiz Data from API
- Display First Quiz Question (Question h1 at top, Answers each h2 .answer if clicked then .correct or .incorrect)
  - Only One Answer Selectable at a time (clicking other Answers should have no effect until moving to next Question)
- At bottom, 2 Buttons. "Back" and "Next". Initially Disabled. Enable "Next" upon AnswerSelection.
  - When "Next" clicked, change Question to next Question. Disable "Next" on Last Question
  - Enable "Back" when on~ First Question (even if Answer not Selected). Return to Previous Question (w/ prev answer selected)

##### ABSTRACTIONS AND ANALOGIES:
- Data Format: { question: "Question...", answers: ["optionOne", "optionTwo", "optionThree"...], correctAnswer: indx}

- GetQuizData: fetch URL. Store Questions in State.
- DisplayQuestion: h1 Question. h2 .answer Answers
- SelectAnswer: Only one at time. Enables NextQuestion (if not Last). Style based on Correctness.
- NextQuestion: Disabled until Answer. Changes Question to Next (if not Last). Disabled Last Question.
- PrevQuestion: Disabled on First Question. Returns to Previous Question showing prev Answer.

##### APPROACH:
- 4 Components: Quiz, Question, Answers, Navigation
- GetQuizData: Create State. Fetch. Store State. ADD selectedAnswer Field (...later, when Answer Selected).
  - Create currentQuestionIndex State storing Index of currentQuestionIndex
- QuizComp: Fetches Quiz Data.
  - DisplayQuestion: Creates QuestionComp and AnswersComp for currentQuestionIndex, then places NavigationComp
- QuestionComp: h1 with Question Content
- AnswersComp: h2 with Answers ITERATED over -> map
  - SelectAnswer: onClick. UPDATES Question STATE's selectedAnswer. Adds to classList (??)
- NavigationComp: button. Initially Disabled.
  - NextQuestion: If Enabled... Update currentQuestionIndex State. Disable until Question State's selectedAnswer filled
  - PrevQuestion: If Enabled... Update currentQuestionIndex State. Disabled if currentQuestionIndex Index == 0

*/

export default function Quiz() {
  const [questions, setQuestions] = useState( [] );
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState( 0 );
  const [chosenAnswers, setChosenAnswers] = useState( [] );

  useEffect( () => {

    const fetchData = async () => {

      let questionsResponse;
      if( QUIZ_API_BASE_URL ) {
        questionsResponse = await fetch(QUIZ_API_BASE_URL).catch(error => {
          console.group();
          console.log("ERROR during Fetch");
          console.log(error);
          console.groupEnd();
        });
      }
      else {
        questionsResponse = await Promise.resolve( {json: () => MOCK_QUIZ} ); // MOCK FOR TESTING
      }

      const questionsJSON = await questionsResponse.json();

      setQuestions( questionsJSON );
    };

    fetchData();
  }, []); // Only OnMount

  //console.log(quizData);
  if( !questions || questions.length === 0 ) return;

  const updateChosenAnswers = (questionIndex, answerIndex) => {
    const newChosenAnswers = [...chosenAnswers];
    newChosenAnswers[questionIndex] = answerIndex;
    setChosenAnswers( newChosenAnswers );
  }

  const currentQuestion = questions[currentQuestionIndex];
  const isFirstQuestion = currentQuestionIndex === 0;
  const isLastQuestion = currentQuestionIndex === questions.length - 1;

  return (
    <ErrorBoundary>
      <h1>{currentQuestion.question}</h1>
      {
        currentQuestion.answers.map( (answer, answerIndex) => {
          const chosenAnswer = chosenAnswers[currentQuestionIndex];
          let className = 'answer';

          if( chosenAnswer === answerIndex ) {
            className += " "; // Explicit space for next Class Name
            className += currentQuestion.correctAnswer === chosenAnswer ? "correct" : "incorrect";
          }

          return (
            <h2
              key={answer}
              className={className}
              onClick={() => {
                if( chosenAnswer != null ) return; // Currently only supporting one answer selection, period.
                updateChosenAnswers( currentQuestionIndex, answerIndex );
              }}>
              {answer}
            </h2>
          );
        })
      }

      <button
        disabled={isFirstQuestion}
        onClick={() => {
          setCurrentQuestionIndex( currentQuestionIndex - 1 );
        }}>
        Back
      </button>

      <button
        disabled={isLastQuestion || chosenAnswers[currentQuestionIndex] == null}
        onClick={() => {
          setCurrentQuestionIndex( currentQuestionIndex + 1 );
        }}>
        Next
      </button>

    </ErrorBoundary>
  );
}


class ErrorBoundary extends Component {
  state = { hasError: false, error: null };

  static getDerivedStateFromError(error) {
    // Error in a Child occurred
    console.group();
    console.log("ERROR");
    console.log(error)
    console.groupEnd();
    return { hasError: true };
  }

  render() {
    if( this.state.hasError ) {
      return (
        <>
          <h1>Error... That's alright! You'll get there!</h1>;
        </>
      );
    }

    return this.props.children;
  }

}
