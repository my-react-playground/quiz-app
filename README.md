#################################################
################## AUTHOR NOTES ##################
#################################################

**See src/App.js for App-specific Code**

##### OBJECTIVE:
- A simple Quiz App to practice React Basics and Interacting with the Server


##### NOTE:
- First React App, meant as a way to practice the main Concepts.
  - May consider Context later, Optimizations with Memo, or other Concepts.
  - Also, breaking up Components into separate files, more robust error handling, etc.
- First pass is basic Implementation, not breaking out into Components yet.
- In the rough Design below, the "Approach" is Desired/Planned State, but **not complete yet**


##### HOW TO USE:
In App's Root Directory:
- Run "npm install"
- Run "npm start"


/
### Design

##### REQUIREMENTS:
- Obtain Quiz Data from API
- Display First Quiz Question (Question h1 at top, Answers each h2 .answer if clicked then .correct or .incorrect)
  - Only One Answer Selectable at a time (clicking other Answers should have no effect until moving to next Question)
- At bottom, 2 Buttons. "Back" and "Next". Initially Disabled. Enable "Next" upon AnswerSelection.
  - When "Next" clicked, change Question to next Question. Disable "Next" on Last Question
  - Enable "Back" when on~ First Question (even if Answer not Selected). Return to Previous Question (w/ prev answer selected)

##### ABSTRACTIONS AND ANALOGIES:
- Data Format: { question: "Question...", answers: ["optionOne", "optionTwo", "optionThree"...], correctAnswer: indx}

- GetQuizData: fetch URL. Store Questions in State.
- DisplayQuestion: h1 Question. h2 .answer Answers
- SelectAnswer: Only one at time. Enables NextQuestion (if not Last). Style based on Correctness.
- NextQuestion: Disabled until Answer. Changes Question to Next (if not Last). Disabled Last Question.
- PrevQuestion: Disabled on First Question. Returns to Previous Question showing prev Answer.

##### APPROACH:
- 4 Components: Quiz, Question, Answers, Navigation
- GetQuizData: Create State. Fetch. Store State. ADD selectedAnswer Field (...later, when Answer Selected).
  - Create currentQuestionIndex State storing Index of currentQuestionIndex
- QuizComp: Fetches Quiz Data.
  - DisplayQuestion: Creates QuestionComp and AnswersComp for currentQuestionIndex, then places NavigationComp
- QuestionComp: h1 with Question Content
- AnswersComp: h2 with Answers ITERATED over -> map
  - SelectAnswer: onClick. UPDATES Question STATE's selectedAnswer. Adds to classList (??)
- NavigationComp: button. Initially Disabled.
  - NextQuestion: If Enabled... Update currentQuestionIndex State. Disable until Question State's selectedAnswer filled
  - PrevQuestion: If Enabled... Update currentQuestionIndex State. Disabled if currentQuestionIndex Index == 0




/


/

####################################################
################## React-Specific Notes ##################
####################################################

# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
